"""
An extension that helps to reload Django server by simple update of serial number file

Developed by hilsein@gmail.com

"""

try:
    import django 
except ImportError:  # pragma: no cover
    pass
else:
    pass

__version__ = VERSION = (0, 0, 3)
