from __future__ import absolute_import

import os
import imp

from datetime import datetime
from random import randint

from django import forms
from django.conf import settings

DEFAULT_CONFIG = {
    'basename': 'serial.py',
    'dirname': os.path.dirname(os.path.realpath(__file__))
}

def gen_serial():
    return int(str(datetime.now().strftime("%Y%m%d%H%M%S")) + str(randint(100, 999)))

class SNReloader(object):
    def __init__(self, config_name='default', *args, **kwargs):
        # Setup config from defaults.
        self.config = DEFAULT_CONFIG.copy()

        # Try to get valid config from settings.
        configs = getattr(settings, 'SNRELOADER_CONFIGS', None)
        if configs:
            if isinstance(configs, dict):
                # Make sure the config_name exists.
                if config_name in configs:
                    config = configs[config_name]
                    # Make sure the configuration is a dictionary.
                    if not isinstance(config, dict):
                        raise ImproperlyConfigured('SNRELOADER_CONFIGS["%s"] \
                                setting must be a dictionary type.' %
                                                   config_name)
                    # Override defaults with settings config.
                    self.config.update(config)
                else:
                    raise ImproperlyConfigured("No configuration named '%s' \
                            found in your SNRELOADER_CONFIGS setting." %
                                               config_name)
            else:
                raise ImproperlyConfigured('SNRELOADER_CONFIGS setting must be a\
                        dictionary type.')

    def get_full_path(self):
        return os.path.join(self.config['dirname'], self.config['basename'])


    def get_mod_name_file_ext(self):
        return os.path.splitext(os.path.split(self.get_full_path())[-1])

    def load_module(self):
        mod_name, file_ext = self.get_mod_name_file_ext()
        fp, pathname, description = imp.find_module(mod_name, [self.config['dirname']])
        return imp.load_module(mod_name, fp, pathname, description)

    def update_serial(self):
        serial = gen_serial()
        with open(self.get_full_path(), 'w') as outfile:
            line = "SERIAL = {}\n".format(serial)
            outfile.write(line)
        return serial

