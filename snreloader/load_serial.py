from __future__ import absolute_import

from .base import SNReloader

def load_serial(config_name='default'):
    sn_reloader = SNReloader(config_name=config_name)
    _mod = sn_reloader.load_module()
    return _mod.SERIAL
