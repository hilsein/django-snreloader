from __future__ import absolute_import

from snreloader.base import SNReloader

def update_serial(config_name='default'):
    sn_reloader = SNReloader(config_name=config_name)
    return sn_reloader.update_serial()

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        update_serial(sys.argv[1])
    else:
        update_serial()

