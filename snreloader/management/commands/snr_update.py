from django.core.management.base import BaseCommand, CommandError
from snreloader.update_serial import update_serial

class Command(BaseCommand):
    help = "Show serial number"

    def add_arguments(self, parser):
        parser.add_argument('config_name', nargs='?', default='default')

    def handle(self, *args, **options):
        self.stdout.write(str(update_serial(options['config_name'])))
