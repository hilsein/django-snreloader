from setuptools import setup, find_packages
from snreloader import __version__

# Load in babel support, if available.
try:
    from babel.messages import frontend as babel
    cmdclass = {
        "compile_catalog": babel.compile_catalog,
        "extract_messages": babel.extract_messages,
        "init_catalog": babel.init_catalog,
        "update_catalog": babel.update_catalog,
    }
except ImportError:
    cmdclass = {}

setup(
    name="django-snreloader",
    version='.'.join(str(x) for x in __version__),
    license="BSD",
    description="An extension that helps to reload Django server by simple update of serial number file",
    author="Alex Hilsein",
    author_email="hilsein@gmail.com",
    url="http://bitbucket.org/hilsein/django-snreloader.git",
    zip_safe=False,
    packages=find_packages(),
    package_data={
        "snreloader": ["locale/*/LC_MESSAGES/django.*",]},
    cmdclass=cmdclass,
    install_requires=[
        "django>=1.8",
    ],
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        "Framework :: Django",
    ]
)
