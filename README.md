# django-snreloader #

**django-snreloader** is an extension that helps to reload Django server by simple update of serial number file


Installation
============

* git clone https://bitbucket.org/hilsein/django-snreloader
* cd django-snreloader
* python setup.py install

Usage
=====

* Add 'snreloader' to INSTALLED_APPS:


```
#!python

  INSTALLED_APPS = [
    ...
    'snreloader',
    ...
  ]
```

* Create directory to store a serial number python file:

```
  yourproject/
    app1/
    app2/
    ...
    snreload_dir/
```

* Make sure that created directory has permissions to create/write a serial number python file:

```
  chown www-data:www-data yourproject/snrealod_dir
  chmod 775 yourproject/snreload_dir
```

* Add to your project's settings.py following dict:

```
#!python

SNRELOADER_CONFIGS = {
    'default': {
        'basename': 'my_serial.py',
        'dirname': os.path.join(BASE_DIR, 'snreload_dir'),
    },
    'test': {
        'basename': 'test_serial.py',
        'dirname': os.path.join(BASE_DIR, 'snreload_dir'),
    },
}

```

* Use the following code to update serial number:


```
#!python

from snreloader.update_serial import update_serial

update_serial()


```

or 

```
#!python

from snreloader.update_serial import update_serial

update_serial('test')


```

where 'test' argument is a config name



* Use the following code in a file that should be reloader in case of serial update:


```
#!python

from snreloader.load_serial import load_serial

load_serial()

```

or

```
#!python

from snreloader.load_serial import load_serial

load_serial('test')

```

where 'test' argument is a config name

* Use django command line to manage serials:

```
python manage.py snr_show [config_name]
python manage.py snr_update [config_name]

```
